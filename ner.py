from denver.learners import FlairSequenceTaggerLearner

model_path = "./ApolloEntityExtractor_0.18.pt"
learn = FlairSequenceTaggerLearner(mode='inference', model_path=model_path)

# Get prediction
data_df = learn.predict_on_df(data='./t.csv', text_cols='sentence')

data_df.to_csv('outfile.csv', index=False, encoding='utf-8')

