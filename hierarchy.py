from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from scipy.cluster.hierarchy import ward, dendrogram, linkage
import collections, numpy
from scipy.cluster.hierarchy import cophenet
from scipy.spatial.distance import pdist
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import fcluster
from sklearn.cluster import KMeans
import pandas as pd
from pyvi import ViTokenizer
import string




database = []

data = pd.read_csv('data_kcloset/queryKB.csv')
data = list(data['sentence'])
for d in data:
    line = ''.join([i for i in str(d) if not i.isdigit()])
    database.append(line)


tfidf_vectorizer = TfidfVectorizer()
tfidf_matrix = tfidf_vectorizer.fit_transform(database)
print(tfidf_matrix)
terms = tfidf_vectorizer.get_feature_names()
dist = cosine_similarity(tfidf_matrix)
linkage_matrix = ward(dist)


def fancy_dendrogram(*args, **kwargs):
    max_d = kwargs.pop('max_d', None)
    if max_d and 'color_threshold' not in kwargs:
        kwargs['color_threshold'] = max_d
    annotate_above = kwargs.pop('annotate_above', 0)

    ddata = dendrogram(*args, **kwargs)

    if not kwargs.get('no_plot', False):
        plt.title('Hierarchical Clustering Dendrogram (truncated)')
        plt.xlabel('sample index or (cluster size)')
        plt.ylabel('distance')
        for i, d, c in zip(ddata['icoord'], ddata['dcoord'], ddata['color_list']):
            x = 0.5 * sum(i[1:3])
            y = d[1]
            if y > annotate_above:
                plt.plot(x, y, 'o', c=c)
                plt.annotate("%.3g" % y, (x, y), xytext=(0, -5),
                             textcoords='offset points',
                             va='top', ha='center')
        if max_d:
            plt.axhline(y=max_d, c='k')
    return ddata

max_d = 3
clusters = fcluster(linkage_matrix, max_d, criterion='distance')
print(clusters)
k = collections.Counter(clusters)
print(k)
print(len(k))
num_clusters = len(k)
km = KMeans(n_clusters=num_clusters)
km.fit(tfidf_matrix)
clusters_kmean = km.labels_.tolist()


ax = fancy_dendrogram(
    linkage_matrix, 
    leaf_rotation=90., 
    leaf_font_size=12.,
    max_d = max_d
    )
label = []
# print(ax)
for i in ax['color_list']:
    label.append(i)




# calculate full dendrogram
plt.show()

datan = []

data_sentence = pd.read_csv('data_kcloset/queryKB.csv')
filedata = list(data_sentence['sentence'])

for x in filedata:
    datan.append(x)


def tokenize_and_stem(text):
    stems = text.lower().split(' ')
    return stems

def tokenize_only(text):
    filtered_tokens = text.lower().split(' ')
    return filtered_tokens

totalvocab_stemmed = []
totalvocab_tokenized = []
for i in database:
    allwords_stemmed = tokenize_and_stem(i)
    totalvocab_stemmed.extend(allwords_stemmed)
    allwords_tokenized = tokenize_only(i)
    totalvocab_tokenized.extend(allwords_tokenized)
vocab_frame = pd.DataFrame({'words': totalvocab_tokenized}, index = totalvocab_stemmed)
print('there are ' + str(vocab_frame.shape[0]) + ' items in vocab_frame')

films = {'database': database, 'cluster': clusters }
print(clusters)
frame = pd.DataFrame(films, index = [clusters] , columns = ['cluster'])


ne = []
til = []
order_centroids = km.cluster_centers_.argsort()[:, ::-1]
print(vocab_frame)
print(terms)
for i in range(len(k)):    
    for ind in order_centroids[i, :6]:
        print(ind)
        print(type(ind))
        a = (vocab_frame.loc[terms[ind].split(' ')].values.tolist()[0][0])
        ne.append(a)
    # print(ne)
    titlie = ''
    for i in ne:
        titlie += str(i + ', ')
    til.append(titlie)
    ne = []


n = clusters_kmean
ns = len(n)
for i in range(len(k)):
    with open('data_kcloset/data/' + str(i) +'.txt', 'a') as fileUrl:
        fileUrl.writelines(til[i] + "\n" + "\n")
    
    for j in range(ns):
        if i == n[j]:
            with open('data_kcloset/data/' + str(i) +'.txt', 'a') as fileUrl:
                fileUrl.writelines(str(datan[j]) + "\n")
    
    # print("Cluster %d titles:" % i, end='')
    # for title in frame.loc[i]['title'].values.tolist():
    #     print(' %s,' % title, end='')


