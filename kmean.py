"""
This is a simple application for sentence embeddings: clustering
Sentences are mapped to sentence embeddings and then k-mean clustering is applied.
"""
from sentence_transformers import SentenceTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import MiniBatchKMeans
import pandas as pd
import csv
import re
from unicodedata import normalize as nl
import glob

embedder = SentenceTransformer('distilbert-multilingual-nli-stsb-quora-ranking')

# Corpus with example sentences

path = r'./data_kcloset/data'
path_data = glob.glob(path + "/*.txt")

corpus = []
for file in path_data:
    with open(file, 'r') as fh:
        for line in fh:
            line = line.replace("\n", "")
            corpus.append(str(line))


# corpus = list(file_data['sentence'])


def normalize(text, rm_emoji: bool= True, rm_url: bool = True, rm_special_token: bool = True):
    """Function to normalize text

	:param text: The text to normalize
	:param rm_emoji: If True, replace the emoji token into <space> (" ")
	:param rm_url: If True, replace the url token into <space> (" ")
	:param rm_special_token: If True, replace the special token into <space> (" ")
	:returns: txt: The text after normalize.
	"""

    # Convert input to UNICODE utf-8
    txt = nl("NFKC", text).lower()

    # Remove emoji
    if rm_emoji:
        emoji_pattern = re.compile(
            "["
            "\U0001F600-\U0001F64F"  # emoticons
            "\U0001F300-\U0001F5FF"  # symbols & pictographs
            "\U0001F680-\U0001F6FF"  # transport & map symbols
            "\U0001F1E0-\U0001F1FF"  # flags (iOS)
            "]+",
            flags=re.UNICODE,
        )
        txt = emoji_pattern.sub(r" ", txt)

    # Remove url, link
    if rm_url:
        url_regex = re.compile(r"\bhttps?://\S+\b")
        txt = url_regex.sub(r" ", txt)

    # Remove special token and duplicate
    if rm_special_token:
        txt = re.sub(
            r"[^a-z0-9A-Z*\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠẾếàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]",
            " ",
            txt,
        )
        txt = re.sub(r"\s{2,}", " ", txt)

    return txt


nor_corpus = [normalize(str(sentence)) for sentence in corpus]
normalize_corpus = []
for d in nor_corpus:
    line = ''.join([i for i in str(d) if not i.isdigit()])
    normalize_corpus.append(line)
corpus_embeddings = embedder.encode(normalize_corpus)

# Perform kmean clustering
num_clusters = 10
clustering_model = MiniBatchKMeans(n_clusters=num_clusters)
clustering_model.fit(corpus_embeddings)
cluster_assignment = clustering_model.labels_.tolist()

clustered_sentences = [[] for i in range(num_clusters)]
for sentence_id, cluster_id in enumerate(cluster_assignment):
    clustered_sentences[cluster_id].append(corpus[sentence_id])

for i, cluster in enumerate(clustered_sentences):
    print("Cluster ", i + 1)
    print(cluster)
    print("")


def tokenize_and_stem(text):
    stems = text.lower().split(' ')
    return stems


def tokenize_only(text):
    filtered_tokens = text.lower().split(' ')
    return filtered_tokens


totalvocab_stemmed = []
totalvocab_tokenized = []
for i in normalize_corpus:
    allwords_stemmed = tokenize_and_stem(i)
    totalvocab_stemmed.extend(allwords_stemmed)
    allwords_tokenized = tokenize_only(i)
    totalvocab_tokenized.extend(allwords_tokenized)
vocab_frame = pd.DataFrame({'words': totalvocab_tokenized}, index=totalvocab_stemmed)
print('there are ' + str(vocab_frame.shape[0]) + ' items in vocab_frame')

films = {'database': normalize_corpus, 'cluster': cluster_assignment}
frame = pd.DataFrame(films, index=[cluster_assignment], columns=['cluster'])

path_n = r'./data_kcloset/data'
path_data_n = glob.glob(path + "/*.txt")

datan = []
filedata = []
for file in path_data_n:
    with open(file, 'r') as fh:
        for line in fh:
            line = line.replace("\n", "")
            filedata.append(str(line))

# datan = []
# data_sentence = pd.read_csv('data_kcloset/test.csv')
# filedata = list(data_sentence['sentence'])

for x in filedata:
    datan.append(x)

n = cluster_assignment
ns = len(n)
for i in range(num_clusters):
    for j in range(ns):
        if i == n[j]:
            with open('data_kcloset/data_output/' + str(i) + '.txt', 'a') as fileUrl:
                fileUrl.writelines(str(datan[j]) + "\n")
